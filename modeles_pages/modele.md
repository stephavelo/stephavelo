Title: <titre>
Slug: <slug>
Date: <date> <heure>
Modified:  <date> <heure>
Category: <categorie>
Tags: <tags>

Authors: <auteur>
Summary: <resume>

# Titre 1  #

## Titre 2  ##

[texte du lien](https://adresse_du_lien/) 

- liste
    - sous liste
 
 [an image]({static}/images/velo.png)
 
> citation

<!-- PELICAN_END_SUMMARY -->

	code doit être après pelican end summary