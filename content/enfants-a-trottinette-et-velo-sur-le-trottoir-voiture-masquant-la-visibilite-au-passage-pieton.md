Title: Enfants à trottinette et vélo sur le trottoir de gauche, voiture plus haute qu'eux masquant la visibilité du passage piéton sur le trottoir de droite
Slug: enfants-a-trottinette-et-velo-sur-le-trottoir-voiture-masquant-la-visibilite-au-passage-pieton
Date: 2021-01-03 21:31:48
Modified:  2021-01-03 21:31:48
Category: vélo
Tags: piétons, vélo, visibilité, enfants

Authors: steph
Summary: Enfants à trottinette et vélo sur le trottoir de gauche, voiture sur l'autre trottoir masquant le passage piéton, plus haute que les enfants

C'était un jour de pluie, la preuve que les enfants, comme les autres cyclistes, [ne sont pas en sucre ;-)](https://jeanneavelo.fr/2020/01/09/vous-netes-pas-en-sucre/) 

[![Enfants sur le trottoir de gauche, voiture plus haute qu'eux masquant le passage piéton à droite](https://i.imgur.com/9sQ1JdRm.jpg)](https://i.imgur.com/9sQ1JdR.jpg Enfants sur le trottoir de gauche, voiture plus haute qu'eux masquant le passage piéton à droite)

<!-- PELICAN_END_SUMMARY -->

[![Enfants sur le trottoir de gauche, voiture plus haute qu'eux masquant le passage piéton à droite](https://i.imgur.com/nAzlQX9m.jpg)](https://i.imgur.com/nAzlQX9.jpg Enfants sur le trottoir de gauche, voiture plus haute qu'eux masquant le passage piéton à droite)
