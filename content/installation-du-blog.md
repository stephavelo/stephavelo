Title: Installation du blog
Slug: installation-du-blog
Date: 2019-04-28 16:01:59
Modified: 2019-04-28 20:31:59
Category: pelican
Tags: pelican, gitlab, framagit

Authors: steph
Summary: résumé pour l'index et les flux rss

# Choix techniques  #

## Moteur : Pélican  ##

J'ai choisi le moteur de blog [Pelican](https://blog.getpelican.com/)  pour :

- le site [statique](https://blog.victor-hery.com/2017/07/passage-blog-statique.html) : 
    - pas besoin de s'[nquiéter de la sécurité](https://www.bortzmeyer.org/generateurs-web-statiques.html)  et vérifier les mises à jour de Wordpress etc
    - possibilité de l'héberger gratuitement, sur les Gitlab[/Github](https://connect.ed-diamond.com/GNU-Linux-Magazine/GLMF-184/Utiliser-Pelican-comme-moteur-de-blog) Pages - j'utilise Framagit
- les nombreux [plugins](https://github.com/getpelican/pelican-plugins) / [thèmes](http://pelicanthemes.com/) 
- le langage python.

Cependant avec les commandes à taper & co, ce n'est [pas pour qui veut juste taper un article](https://joachimesque.com/blog/2018-01-25-generateurs-de-sites-statiques-c-est-toujours-le-bordel)  et glisser des photos dans une interface 

<!-- PELICAN_END_SUMMARY -->

## Registrar : Gandi  ##

J'ai choisi d'avoir un nom de domaine et de le prendre chez [Gandi](www.gandi.net) pour l'[éthique](https://fr.wikipedia.org/wiki/Gandi_(entreprise)).

## Hébergement : Framagit ##

Je ne voulais pas de Github, racheté par Microsoft, donc ai en premier lieu fait le choix des *Gitlab pages*, puis ai découvert qu'une instance de Gitlab, comprenant les pages, est hébergée par l'association Framasoft. Cette instance s'appelle [Framagit](https://framagit.org/).

L'association [Framasoft](https://framasoft.org/fr/), que j'ai connue par son annuaire de logiciels libres, a mené [beaucoup d'autres actions](https://fr.wikipedia.org/wiki/Framasoft#Historique_du_r%C3%A9seau)  notamment la campagne [dégooglisons internet](https://degooglisons-internet.org/) sur laquelle elle est revenue sur [son blog](https://framablog.org/2017/09/25/degooglisons-internet-cest-la-fin-du-debut/).
 
C'est grâce à Framasoft que les *Gitlab Pages* ont été libérées : l'histoire est racontée sur [linuxfr](https://linuxfr.org/news/gitlab-libere-les-gitlab-pages).

# Création du blog #

J'ai largement suivi le tutoriel du blog [fullstackpython](https://www.fullstackpython.com/blog/generating-static-websites-pelican-jinja2-markdown.html) et celui de [mister-gold](https://mister-gold.pro/posts/en/deploy-pelican-on-gitlab-pages/).

Ne refaisant pas les manipulations en écrivant ce texte, je ne prétends pas à l'exhaustivité et il est possible que certaines erreurs soient présentes.

## Création d'un projet sur framagit ##

Je me créé un compte sur Framagit, de nom d'utilisateur *stephavelo*, et créé un projet de même nom pour qu'il soit accessible par l'url *stephavelo.frama.io*. 

Au début j'avais mis sa visibilité en *private* mais pour la génération par Framagit du certificat *Let's Encrypt* (pour le nom de domaine perso) je l'ai plus tard passé en *public*.

### Génération d'une clé ssh ###

Via [l'aide de Framagit](https://framagit.org/help/ssh/README#generating-a-new-ssh-key-pair), on génère la clé avec la commande

	ssh-keygen -t ed25519 -C "<monadressemailframagit>"

## Création d'un environnement virtuel et installation de Pélican ##

Après avoir créé un dossier ~/Documents/blog/, j'y installe pelican et markdown dans un environnement virtuel :
	
	python3 -m venv staticblog
	source staticblog/bin/activate
	pip install pelican markdown
	
Je clone ensuite le répertoire du projet framagit dans lequel je vais stocker le blog et y lance l'assistant de pelican :

	git clone https://framagit.org/stephavelo/stephavelo.git
	cd stephavelo/
	pelican-quickstart
	
## Configuration de Pélican pour la publication ##
	
Le répertoire de sortie est par défaut *output* mais pour publier sur les pages de Framagit (Gitlab) il faut qu'il soit configuré en *public* : 

Ajouter

	OUTPUT_PATH = 'public/'

dans *pelicanconf.py* et *publishconf.py* puis remplacer

	OUTPUTDIR=$(BASEDIR)/output
	
par

	OUTPUTDIR=$(BASEDIR)/public
	
dans le *Makefile*

## Test de Pélican en local ##

On créé d'abord un article dans le répertoire *content*, exemple

	Title: Mon premier article
	Date: 2019-04-27 17:25
	Modified: 2019-04-27 17:25
	Category: python
	Tags: pelican, gitlab
	Slug: mon-premier-article
	Authors: steph
	Summary: résumé pour l'index et les flux rss

	Ici mon contenu	

On peut générer le blog avec la commande

	make html
	
puis ouvrir un serveur http de test à l'adresse 

	http://localhost:8000
	
avec la commande
	
	make devserver

## Configuration pour la publication via les *Gitlab pages* ##

### Création des fichiers de configuration ###

Pour la création des fichiers requirements.txt, .gitlab-ci.yml et  .gitignore j'ai repris ce qui est dit par [mister-pro](https://mister-gold.pro/posts/en/deploy-pelican-on-gitlab-pages/) sous forme d'un script bash, ma paresse aidant :

	#!/bin/bash
	# https://mister-gold.pro/posts/en/deploy-pelican-on-gitlab-pages/

	project_folder="/home/stephbuntu/Documents/blog/stephavelo"
	echo ${project_folder}

	# requirements.txt
	fichier_requirements=${project_folder}/requirements.txt

	cat > ${fichier_requirements} << EOL
	pelican
	markdown
	EOL

	# .gitlab-ci.yml continuous integration
	fichier_yml=${project_folder}/.gitlab-ci.yml

	cat > ${fichier_yml} << EOL
	image: python:3.6-alpine

	pages:
	  script:
	  - pip install -r requirements.txt
	  - pelican -s publishconf.py
	  artifacts:
	    paths:
	    - public/
	EOL

	# gitignore

	fichier_gitignore=${project_folder}/.gitignore

	cat > ${fichier_gitignore} << EOL
	public/
	*.pyc
	EOL



### Activation de l'intégration continue ###

D'après la [documentation de Gitlab](https://docs.gitlab.com/ee/ci/enable_or_disable_ci.html) l'intégration continue est active pour toutes les nouvelles installations. Il faut juste avoir un fichier valide [ .gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/README.html) à la racine du projet.

# Publication du blog #

Il n'y a plus qu'à vérifiier les fichiers modifiés, commiter et pusher avec git :

	git add .
	git status
	git commit -m "Création"
	git push origin master
	
# Nom de domaine perso  #

J'ai utilisé le [forum framacolibri](https://framacolibri.org/t/framagit-page-et-nom-de-domaine-perso-resolue/2596) , la [doc de Framasoft](https://docs.framasoft.org/fr/gitlab/gitlab-pages.html#comment-%C3%A7a-marche-), un [post de Framasky](https://framapiaf.org/@framasky/100123898019893205) et le [forum de gitlab](https://forum.gitlab.com/t/how-can-i-change-project-visibility/3600/5)  pour trouver comment changer la visibilité d'un projet.
	
## Acheter un domaine  ##

Préalable au reste ;-)

## Ajouter le domaine dans Framagit  ##

On va suivre la [procédure de Framasoft](https://docs.framasoft.org/fr/gitlab/gitlab-pages.html#utiliser-un-domaine-personnalis%C3%A9-accessible-en-https).

> Allez dans Paramètres / Pages et cliquez sur le bouton New Domain en haut à droite, puis remplissez seulement le champ Domain et laissez le reste vide pour l'instant, puis cliquez sur Create New Domain.

En cliquant sur *Create new domain* j'obtiens une erreur :

> Certificate must be present if HTTPS-only is enabled. 
> 
> Key must be present if HTTPS-only is enabled.

En allant dans les *settings* des pages je vois que la case *Force HTTPS (requires valid certificate)* est cochée, je la décoche. En retournant dans l'ajout de nom de domaine je peux maintenant entrer mon domaine, cliquer sur *Create New Domain* et j'obtiens des configurations DNS :

	stephavelo.fr CNAME stephavelo.frama.io

Cependant, la configuration que j'obtiens est une *CNAME* or j'ai déjà une adresse mail pour ce domaine chez Gandi, je ne peux donc pas utiliser cette configuration, comme le dit JosephK sur  le [forum framacolibri](https://framacolibri.org/t/framagit-page-et-nom-de-domaine-perso-resolue/2596) : 

 > JosephK 2018-03-15 14:43:20 UTC #2
> 
> En fait la [doc de Gitlab](https://framagit.org/help/user/project/pages/getting_started_part_three.md#tldr)  précise que si le nom de domaine sert pour plusieurs services (dont le mail que Gandi propose par défaut dans ses DNS) alors il faut utiliser l’adresse IP (enregistrement A et AAAA) au lieu du CNAME et ajouter l’enregistrement TXT au niveau du domaine et pas du sous domaine « _gitlab-pages-verification-code.exemple.org. »

Si j'essaie d'ajouter cet enregistrement DNS dans l'interface de Gandi, j'obtiens une erreur :

> Erreur : CNAME record must be the only record for @

Alors qu'avant modification j'ai en A et TXT
	
	@ 10800 IN A 217.70.184.38
	@ 10800 IN TXT "v=spf1 include:_mailcust.gandi.net ?all"

Je fais comme Plumf et modifie les enregistrements A et AAAA vers les IP des pages Framagit en IPV4 et IPV6, et l'enregistrement TXT avec le code de vérification donné lors de la création du domaine dans Framagit :

	@ 1800 IN A 144.76.206.44
	@ 1800 IN AAAA 2a01:4f8:200:1302::44
	@ 1800 IN TXT "gitlab-pages-verification-code=<mon_code_de_verif>"

Ensuite comme dit dans la [procédure de Framasoft](https://docs.framasoft.org/fr/gitlab/gitlab-pages.html#utiliser-un-domaine-personnalis%C3%A9-accessible-en-https)  on revient dans la gestion du domaine dans Framagit, on clique sur le bouton *Vérifier* et il passe en vert.

## Accéder au site en HTTPS sans avertissement  ##

En accédant au site avec le nom de domaine perso, il est automatiquement redirigé en https (ou alors c'est parce que j'ai https everywhere ?) et Firefox affiche un énorme avertissement. Pas cool.
 
La [procédure de Framasoft](https://docs.framasoft.org/fr/gitlab/gitlab-pages.html#utiliser-un-domaine-personnalis%C3%A9-accessible-en-https) et un [post de Framasky](https://framapiaf.org/@framasky/100123898019893205) m'apprennent qu'un script génère un certificat toutes les heures mais que pour cela il faut que :

  - au moins une page web soit déployée (ça c'est bon)
  - le script puisse accéder au dépôt, il faut donc que je le passe en public.

Le [forum de gitlab](https://forum.gitlab.com/t/how-can-i-change-project-visibility/3600/5) m'apprend que pour changer la visibilité d'un projet il faut ajouter */edit* à son url. Je me rends donc sur la page 

	https://framagit.org/stephavelo/stephavelo/edit
	
et change la visibilité du projet à *public*.

Une heure après, enfin à la prochaine heure de la forme HH:38, le site s'affiche sans avertissement.

Et voilà, prochaine étape les thèmes/plugins :)