Title: Piétonne manifestant l'envie de s'engager sur un passage piéton mais masquée par la voiture garée juste avant
Slug: pietonne-manifestant-l-envie-de-s-engager-sur-un-passage-pieton-mais-masquee-par-la-voiture-garee-juste-avant
Date: 2021-01-03 21:04:42
Modified:  2021-01-03 21:04:42
Category: vélo
Tags: piétons, vélo, vélotaf, visibilité

Authors: steph
Summary: Démonstration par l'exemple de la nécessité de supprimer le stationnement motorisé avant les passages piéton, inscrite dans la LOM et obligatoire d'ici 2026.

[![Piétonne voulant traverser à un passage piéton mais masquée par une voiture](https://i.imgur.com/TOZg8K9m.jpeg)](https://i.imgur.com/TOZg8K9.jpeg Piétonne voulant traverser à un passage piéton mais masquée par une voiture)


<!-- PELICAN_END_SUMMARY -->

[![Piétonne voulant traverser à un passage piéton mais masquée par une voiture](https://i.imgur.com/vMMVg8em.jpg)](https://i.imgur.com/vMMVg8e.jpg Piétonne voulant traverser à un passage piéton mais masquée par une voiture)

[![Piétonne voulant traverser à un passage piéton mais masquée par une voiture](https://i.imgur.com/bdIzMzQm.jpg)](https://i.imgur.com/bdIzMzQ.jpg Piétonne voulant traverser à un passage piéton mais masquée par une voiture)

[![Piétonne voulant traverser à un passage piéton mais masquée par une voiture](https://i.imgur.com/hUnFwMtm.jpeg)](https://i.imgur.com/hUnFwMt.jpeg Piétonne voulant traverser à un passage piéton mais masquée par une voiture)

