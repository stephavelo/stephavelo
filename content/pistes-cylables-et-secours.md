Title: Pistes cyclables et secours
Slug: pistes-cyclables-et-secours
Date: 2021-01-03 13:55:48
Modified:  2021-01-03 13:55:48
Category: vélo
Tags: vélo, vélotaf, vélut

Authors: steph
Summary: Comment éviter aux secours d'être ralentis par les embouteillages ?

# Avant création de piste cyclable #

[![Pompiers bloqués dans un embouteillage](https://i.imgur.com/L6pFrodm.png)](https://i.imgur.com/L6pFrod.png Pompiers bloqués dans un embouteillage)

> “Désolé on n’a pas pu sauver la victime car tout le monde a pris sa voiture ce jour-là”. Sauvez des vies en prenant #legoûtduvélo!

[Source Stein Van Oosteren](https://twitter.com/FARaVelo/status/923448996248653824) 

<!-- PELICAN_END_SUMMARY -->

# Après création de piste cyclable bidirectionnelle #

[![Ambulance passant le bouchon grâce à la nouvelle piste cyclable bidirectionnelle](https://i.imgur.com/dhm5On9m.png)](https://i.imgur.com/dhm5On9.png Ambulance passant le bouchon grâce à la nouvelle piste cyclable bidirectionnelle)

> Vous souvenez-vous de cette ambulance bloquée sur la #rueVercingetorix dans le bouchon habituel? Depuis, une piste cyclable a été créée sur la chaussée. Voici la FLUIDITE que cela apporte. Merci @lololemagicien

[Source Stein Van Oosteren](https://twitter.com/LCyclable/status/1317586474775269378) 

# Après création de coronapiste unidirectionnelle #

<p><a href="https://i.imgur.com/qaRub06.png" title="Pompiers empruntant une coronapiste à Metz">
    <img src="https://i.imgur.com/qaRub06m.png" alt="Pompiers empruntant une coronapiste à Metz" />
  </a></p>

[![Pompiers empruntant une coronapiste à Metz](https://i.imgur.com/qaRub06m.png)](https://i.imgur.com/qaRub06.png Pompiers empruntant une coronapiste à Metz)

> Sinon, les #Coronapiste, ça sert aussi à ça.

[Source Vélotaf Metz](https://twitter.com/Velotaf_Metz/status/1265710053874708481) 