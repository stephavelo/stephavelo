Title: Piétons de nuit sur le trottoir de gauche de l'avenue des Nations de Yutz, très large
Slug: pietons-de-nuit-sur-le-trottoir-de-gauche-de-l-avenue-des-nations-de-yutz-tres-large
Date: 2021-01-03 22:21:40
Modified:  2021-01-03 22:21:40
Category: vélo
Tags: nuit, piétons, vélo, vélotaf, vélut, visibilité

Authors: steph
Summary: Deux photos pour montrer la visibilité, tirées de la GoPro

[![Piétons sur le trottoir de gauche de cette avenue large](https://i.imgur.com/RrWbfdbm.jpg)](https://i.imgur.com/RrWbfdb.jpg Piétons sur le trottoir de gauche de cette avenue large)


<!-- PELICAN_END_SUMMARY -->


[![Piétons sur le trottoir de gauche de cette avenue large](https://i.imgur.com/oXi3ZEkm.jpeg)](https://i.imgur.com/oXi3ZEk.jpeg Piétons sur le trottoir de gauche de cette avenue large)
