Title: Parlons enfin de vélo
Slug: parlons-enfin-de-velo
Date: 2019-07-28 09:09:02
Modified:  2019-07-28 09:09:02
Category: vélo
Tags: vélo, vélotaf, vélut

Authors: steph
Summary: Premier article ne parlant pas des subtilités techniques d'installation de ce blog mais du vrai sujet, le vélo !

# Pourquoi ce blog  #

Avant j'habitais dans un bled paumé, pas idéal pour le vélo :

- dans une cuvette (donc avec que des côtes autour) 
- les routes environnantes avec des virages, donc mauvaise visibilité
- des motorisés roulant vraiment vite, notamment des travailleurs frontaliers au Luxembourg en début et fin de journée en semaine. 

Pas vraiment le coin idéal pour se déplacer quotidiennement à vélo, la voiture est reine et les transports en commun rares (à des horaires scolaires et c'est tout). Les commodités ne sont pas vraiment à côté, et les aménagements cyclables inconnus.

Suite à mon déménagement en ville, le contexte a changé :

- énormément de choses (magasins, travail, déchetterie etc) sont dans un rayon de disons 5-10 km, distance parfaite pour le vélo. 
- étant en agglomération, les vitesses sont souvent limitées à 50 km/h là où je roule ce qui rend le vélo compétitif par rapport à la voiture, car en ville la vitesse moyenne de ce moyen de déplacement est proche de celle du vélo. 
- tout est plat ou presque pour ce que j'ai expérimenté comme trajets en restant à Thionville / Yutz / Manom / Terville / Uckange ; en allant plus loin comme à Fameck ça grimpe, Nilvange aussi et à Algrange encore plus, donc pas besoin d'assistance électrique (avec ma condition physique) ça va très bien avec un simple vélo
- j'habite à proximité d'une véloroute qui me permet notamment d'avoir un meilleur trajet domicile/travail en cas de canicule (le long de la Moselle) ou de volonté de ne pas prendre le même chemin que les motorisés. Elle me permet aussi d'aller chez le vélociste le plus proche. 
- Enfin, il y a des aménagements cyclables ici, contrairement à mon village.

Donc je vais raconter ici mes aventures en vélotaf/vélut.

# Mes vélos  #

- L'historique : un Gitane acheté il y a longtemps, dont je me sers actuellement pour les trajets quotidiens.
- Le nouveau vélotaf, acheté notamment pour l'hiver : acheté sur [les petites annonces d'Ebay en Allemagne](https://www.ebay-kleinanzeigen.de/s-saarland/nabendynamo-nabenschaltung/k0l285), pour son moyeu dynamo et son moyeu à vitesses intégrées Shimano Nexus 7.
- le vélo pour la neige/les concerts/le vol : un VTT Rockrider bas de gamme acheté 20 € sur [LeBonCoin](https://www.leboncoin.fr/) puis équipé à relativement bas prix avec un porte-bagage sur tige de selle, des garde-boues VTT Zefal, un panier et une [attache fourche pour phare](https://www.xxcycle.fr/support-de-lampe-klickfix-1647,,fr.php).
- le vélo de transport : un VTT Rockrider avec des oeillets pour porte-bagages et garde-boues, qui me sert pour le transport de charges avec la remorque Y-Frame, car il a un écrou à l'arrière ce qui augmente la charge tractable dans la remorque.

# Mes trajets  #

- Domicile-travail : 7.5 km en ville en traversant un pont. Possibilité de passer par la véloroute sur certaines portions.
- Courses : 
  	- au retour du travail, magasin sur le trajet.
  	- dans la zone commerciale, passage par le périphérique (limité à 50) puis "piste" sur trottoir.
- Famille :
	- En ville, passage par le périphérique 
	- En campagne, piste cyclable sur le début du trajet
- Concerts / évènements :
	- Certains en ville ou dans la ville d'à côté
	- Certains dans une autre ville joignable par la véloroute avec du trajet ensuite en ville
	- Certains dans une autre ville avec de la route hors agglomération sans aménagement cyclable.