Title: Création d'un nouvel article sous Pélican
Slug: creation-d-un-nouvel-article-sous-pelican
Date: 2019-07-27 17:38:25
Modified:  2019-07-27 17:38:25
Category: pelican
Tags: pelican

Authors: steph
Summary: Comment créer un nouvel article manuellement ou par script
	
# Manuellement  #

Aller dans le répertoire du contenu, créer un fichier .md par exemple mon-article.md, et le remplir avec les métadonnées et le contenu. Exemple avec cet article :

	Title: Création d'un nouvel article sous Pélican
	Slug: creation-d-un-nouvel-article-sous-pelican
	Date: 2019-07-27 17:38:25
	Modified:  2019-07-27 17:38:25
	Category: pelican
	Tags: pelican

	Authors: steph
	Summary: Comment créer un nouvel article manuellement ou par script

	# Titre 1  #

	## Titre 2  ##

	[texte du lien](https://adresse_du_lien/) 

	- liste
	    - sous liste
	  
	> citation
	
Je ne viendrai pas en détail sur la signification des éléments ici.

À l'exception du titre, on peut aussi déterminer les infos à partir du nom de fichier, comme expliqué sur le site du [Zeste de savoir](https://zestedesavoir.com/tutoriels/2497/creer-un-site-web-statique-avec-pelican/generons-nos-premieres-pages/les-metadonnees/#1-dans-lentete-du-fichier). Cependant cela oblige encore à construire le nom de fichier, ou le slug.

# Par script #

Je n'avais pas envie de devoir remplir ces données à la main alors j'ai fait un script utilisant dialog (affichage type ncurses).

## Utilisation ##

J'ai mis les scripts dans un répertoire "scripts".

	cd <chemin_du_blog>
	scripts/cree_article.sh 
	
Le script demande ensuite les éléments dans une interface type ncurses (comme l'installeur de debian) et affiche une fenêtre de vérification à la fin. Si on valide, le nouvel article crée est ensuite ouvert dans Remarkable (un éditeur Markdown).

[![demande du titre](https://i.imgur.com/uRlwx8Cm.png)](https://i.imgur.com/uRlwx8C.png demande du titre)

[![demande de l'auteur](https://i.imgur.com/coKdRKtm.png)](https://i.imgur.com/coKdRKt.png demande de l'auteur)

[![demande des tags](https://i.imgur.com/busMAGVm.png)](https://i.imgur.com/busMAGV.png demande des tags)

[![demande de la catégorie](https://i.imgur.com/dXWcj4Tm.png)](https://i.imgur.com/dXWcj4T.png demande de la catégorie)

[![demande de vérification des données saisies](https://i.imgur.com/lhZ3sbcm.png)](https://i.imgur.com/lhZ3sbc.png demande de vérification des données saisies)

[![Edition dans Remarkable](https://i.imgur.com/KMbKbpym.png)](https://i.imgur.com/KMbKbpy.png Edition dans Remarkable)

[![Visualisation avec le navigateur sur le serveur de dev](https://i.imgur.com/h32mwgem.png)](https://i.imgur.com/h32mwge.png Visualisation avec le navigateur sur le serveur de dev)

## Scripts ###

### cree_article.sh ###

	#!/bin/bash
	#~ https://ftp.traduc.org/doc-vf/gazette-linux/html/2004/101/lg101-P.html
	#~ http://linuxcommand.org/lc3_adv_dialog.php
	
	chemin_script=$(dirname "$0")
	
	########################################################################
	#                          FONCTIONS                                   #
	########################################################################
	
	
	#~ https://stackoverflow.com/questions/5608112/escape-filenames-the-same-way-bash-does-it
	escape()
	{
		x=$(printf '%q' "$1")
		echo $x
	}
	
	#~ ---------------------------------------------------------------------
	#~ |                 lancement de dialog                               |
	#~ ---------------------------------------------------------------------
	
	cree_fichier_retour_dialog()
	{
		fichier_temp_pour_retour_tags=`tempfile 2>/dev/null` || fichier_temp_pour_retour_tags=/tmp/fichier_temp_pour_retour_tags$$
		trap "rm -f $fichier_temp_pour_retour_dialog" 0 1 2 5 15
	}
	
	lance_dialog()
	{
		cree_fichier_retour_dialog
	}
	
	affiche_info()
	{
		titre_dialog="$1"
		message_dialog="$2"
		cree_fichier_retour_dialog
		${chemin_script}/lance_dialog.sh --type=info --titre="$titre_dialog" -m="$message_dialog" 2> $fichier_temp_pour_retour_tags
	}
	
	demande_chaine()
	{
		titre_dialog="$1"
		message_dialog="$2"
		cree_fichier_retour_dialog
		${chemin_script}/lance_dialog.sh --type=chaine --titre="$titre_dialog" -m="$message_dialog" 2> $fichier_temp_pour_retour_tags
	}
	
	demande_ouinon()
	{
		titre_dialog="$1"
		message_dialog="$2"
		cree_fichier_retour_dialog
		${chemin_script}/lance_dialog.sh --type=ouinon --titre="$titre_dialog" -m="$message_dialog" 2> $fichier_temp_pour_retour_tags
	}
	
	demande_selection_multiple()
	{
		titre_dialog="$1"
		message_dialog="$2"
		liste_dialog="$3"
		cree_fichier_retour_dialog
		${chemin_script}/lance_dialog.sh --type=cases_a_cocher --titre="$titre_dialog" -m="$message" --liste="$liste_dialog" 2> $fichier_temp_pour_retour_tags
	}
	
	demande_selection_unique()
	{
		titre_dialog="$1"
		message_dialog="$2"
		liste_dialog="$3"
		cree_fichier_retour_dialog
		${chemin_script}/lance_dialog.sh --type=bouton_radio --titre="$titre_dialog" -m="$message" --liste="$liste_dialog" 2> $fichier_temp_pour_retour_tags
	}
	
	#~ ---------------------------------------------------------------------
	#~ |          demande des propriétés du nouvel article                 |
	#~ ---------------------------------------------------------------------
	
	traite_retour()
	{
		case "$1" in
				"ANNULER")
					echo ANNULER
					exit 1
					;;
				"ECHAP")
					echo ECHAP
					exit 1
					;;	
		esac	
	}
	
	
	#~ TITRE
	demande_titre()
	{
		demande_chaine "Titre :" "Veuillez donner le titre."
		titre=$(cat $fichier_temp_pour_retour_tags)
		traite_retour "$titre"
	}
	
	#~ RESUME
	demande_resume()
	{
		demande_chaine "Résumé :" "Veuillez donner le résumé."
		resume=$(cat $fichier_temp_pour_retour_tags)
		traite_retour "$resume"
	}
	
	#~ TAGS
	demande_tags()
	{
		tags_pour_dialog=$(grep Tags\: content/*|cut -d: -f3-10 | tr -d ','| tr ' ' '\n'|sort|uniq|sed '/^$/d'|tr '\n' '§')
		demande_selection_multiple "Tags :" "Veuillez sélectionner les tags." "$tags_pour_dialog"
		tags=$(cat $fichier_temp_pour_retour_tags)
		tags=$(echo "$tags"|sed 's/ /, /g')
		traite_retour "$tags"
	}
	#~ CATEGORY
	demande_categorie()
	{
		categories_pour_dialog=$(grep Category\: content/*|cut -d: -f3-10 | tr -d ','| tr ' ' '\n'|sort|uniq|sed '/^$/d'|tr '\n' '§')
		demande_selection_unique "Catégorie :" "Veuillez sélectionner la catégorie." "$categories_pour_dialog"
		categorie=$(cat $fichier_temp_pour_retour_tags)
		traite_retour "$categorie"
	}
	#~ AUTEUR
	demande_auteur()
	{
		auteurs_pour_dialog=$(grep Authors\: content/*|cut -d: -f3-10 | tr -d ','| tr ' ' '\n'|sort|uniq|sed '/^$/d'|tr '\n' '§')
		demande_selection_unique "Auteur :" "Veuillez sélectionner l'auteur." "$auteurs_pour_dialog"
		auteur=$(cat $fichier_temp_pour_retour_tags)
		traite_retour "$auteur"
	}
	
	prepare_date()
	{
		#~ https://www.cyberciti.biz/faq/linux-unix-formatting-dates-for-display/
		date_creation=$(date +"%Y-%m-%d")
		heure_creation=$(date +"%T")
		echo $date_creation
		echo $heure_creation
	}
	
	prepare_slug()
	{
		#~ https://serverfault.com/questions/348482/how-to-remove-invalid-characters-from-filenames
		slug=$(echo "$titre" | sed -e 's/[^A-Za-z0-9._-]/_/g')
		#~ https://automatthias.wordpress.com/2007/05/21/slugify-in-a-shell-script/
		#~ https://www.admin-linux.fr/bash-suppression-des-accents-cedilles-etc/
		slug=$(echo -n "${titre}" | sed -e 's/[^[:alnum:]]/-/g' | tr -s '-' | tr A-Z a-z | iconv -f utf8 -t ascii//TRANSLIT)
	 	echo $slug
	}
	
	demande_validation()
	{
		echo $slug
		demande_ouinon "Validation :" "Est-ce que toutes les informations ci-dessous sont correctes ?
		§titre : $titre
		§slug : $slug
		§categorie : $categorie
		§tags : $tags
		§auteur : $auteur
		§resume : $resume
		§date : $date_creation
		§heure : $heure_creation
		"
		confirm=$(cat $fichier_temp_pour_retour_tags)
		case $confirm in
				"ANNULER")
					echo ANNULER
					exit 1
					;;
				"ECHAP")
					echo ECHAP
					exit 1
					;;	
				"")
					echo CONTINUE
					;;
				*)
					affiche_info "cas inconnu" "confirmation inconnue : $confirm"
		esac
	}
	
	
	
	#~ ---------------------------------------------------------------------
	#~ |          demande des propriétés du nouvel article                 |
	#~ ---------------------------------------------------------------------
	copie_modele()
	{
	
		CHEMIN_MODELE=modeles_pages/modele.md
		CHEMIN_ARTICLE=content/${slug}.md
		
		cp ${CHEMIN_MODELE} ${CHEMIN_ARTICLE}
		
		sed -i -- 's#<titre>#'"$titre"'#' ${CHEMIN_ARTICLE}
		sed -i -- 's#<slug>#'"$slug"'#' ${CHEMIN_ARTICLE}
		sed -i -- 's#<date>#'"$date_creation"'#' ${CHEMIN_ARTICLE}
		sed -i -- 's#<heure>#'"$heure_creation"'#' ${CHEMIN_ARTICLE}
		sed -i -- 's#<categorie>#'"$categorie"'#' ${CHEMIN_ARTICLE}
		sed -i -- 's#<tags>#'"$tags"'#' ${CHEMIN_ARTICLE}
		sed -i -- 's#<auteur>#'"$auteur"'#' ${CHEMIN_ARTICLE}
		sed -i -- 's#<resume>#'"$resume"'#' ${CHEMIN_ARTICLE}
		
		remarkable 	${CHEMIN_ARTICLE} &
	}
	
	
	
	########################################################################
	#                           SCRIPT                                     #
	########################################################################
	#~ Crée une nouvelle page/un nouvel article
	
	demande_titre
	prepare_slug
	demande_auteur
	demande_resume
	demande_tags
	demande_categorie
	prepare_date
	demande_validation
	copie_modele





### lance_dialog.sh ###

	#!/bin/bash
	#~ https://ftp.traduc.org/doc-vf/gazette-linux/html/2004/101/lg101-P.html
	#~ http://linuxcommand.org/lc3_adv_dialog.php

	#~ parser d'arguments
	#~ https://gist.github.com/jehiah/855086
	#~ https://stackoverflow.com/questions/18544359/how-to-read-user-input-into-a-variable-in-bash

	escape()
	{
		x=$(printf '%q' "$1")
		echo $x
	}

	usage()
	{
	    echo "Demande d'une chaine de caracteres"
	    echo ""
	    echo "$0"
	    echo  "\t-h --help"
	    echo  "\t-t --titre=$TITRE"
	    echo  "\t--type=$TYPE (ouinon, chaine, bouton_radio ou cases_a_cocher)"
	    echo  "\t-m --message=$MESSAGE (insérer § pour passer au paragraphe suivant)"
	    echo ""
	}

	#~ https://ubuntuforums.org/showthread.php?t=2082936
	Arguments=( "$@" )  # Arguments=( "a b" "c" "d e" )


	parse_arguments()
	{
		while [ "$1" != "" ]; do
			PARAM=`echo $1 | awk -F= '{print $1}'`
			VALUE=`echo $1 | awk -F= '{print $2}'`
			case $PARAM in
				-h | --help)
					usage
					exit
					;;
				--titre | -t)
					TITRE=$VALUE
					;;
				--type)
					TYPE=$VALUE
					;;
				--message | -m)
					MESSAGE="$VALUE"
					#~ escape "$MESSAGE"
					MESSAGE=$(echo "$MESSAGE"|sed 's/§/\n - /g')
					#~ escape "$MESSAGE"
					;;
				--liste | -l)
				#~ ne gere pas les espaces
					ELEMENTS="$VALUE"
					;;
				*)
					echo "ERROR: unknown parameter \"$PARAM\""
					usage
					exit 1
					;;
			esac
			shift
		done	
	}

	construit_tableau()
	{
		ELEMENTS=$(echo $ELEMENTS|tr '§' '\n')
		NB_ITEMS=1
		while read -r elt
		do
			LISTE+=("$elt")
			LISTE+=("$elt")
			LISTE+=("off")
		   
			NB_ITEMS=$((NB_ITEMS+1))
		done <<< "$ELEMENTS"
	}

	prepare_dialog()
	{
		DIALOG=${DIALOG=dialog}
		HAUTEUR=20
		LARGEUR=51

		fichier_temp_pour_retour_dialog=`tempfile 2>/dev/null` || fichier_temp_pour_retour_dialog=/tmp/fichier_temp_pour_retour_dialog$$
		trap "rm -f $fichier_temp_pour_retour_dialog" 0 1 2 5 15
	}

	affiche_dialog_checklist()
	{
		$DIALOG  \
			--backtitle "$TITRE"  \
			--title "$TITRE"  \
			--clear \
			--checklist "$MESSAGE" \
				$HAUTEUR $LARGEUR $NB_ITEMS \
				"${LISTE[@]}" \
				2> $fichier_temp_pour_retour_dialog
	}

	affiche_dialog_radiolist()
	{
		$DIALOG  \
			--backtitle "$TITRE"  \
			--title "$TITRE"  \
			--clear \
			--radiolist "$MESSAGE" \
				$HAUTEUR $LARGEUR $NB_ITEMS \
				"${LISTE[@]}" \
				2> $fichier_temp_pour_retour_dialog
	}

	affiche_dialog_menu()
	{
		$DIALOG  \
			--backtitle "$TITRE"  \
			--title "$TITRE"  \
			--clear \
			--menubox "$MESSAGE" \
				$HAUTEUR $LARGEUR $NB_ITEMS \
				"${LISTE[@]}" \
				2> $fichier_temp_pour_retour_dialog
	}

	affiche_dialog_inputbox()
	{
		$DIALOG  \
			--backtitle "$TITRE"  \
			--title "$TITRE"  \
			--clear \
			--inputbox "$MESSAGE" \
				$HAUTEUR $LARGEUR \
				2> $fichier_temp_pour_retour_dialog
	}

	affiche_dialog_yesno()
	{
		$DIALOG  \
			--backtitle "$TITRE"  \
			--title "$TITRE"  \
			--clear \
			--yesno "$MESSAGE" \
				$HAUTEUR $LARGEUR \
				2> $fichier_temp_pour_retour_dialog
	}

	affiche_erreur_type()
	{
		$DIALOG  \
			--backtitle "Erreur de type"  \
			--title "Erreur de type"  \
			--clear \
			--msgbox "type $TYPE inexistant." \
				$HAUTEUR $LARGEUR \
				2> $fichier_temp_pour_retour_dialog
	}


	affiche_dialog_info()
	{
		$DIALOG  \
			--backtitle "$TITRE"  \
			--title "$TITRE"  \
			--clear \
			--msgbox "$MESSAGE" \
				$HAUTEUR $LARGEUR  \
				2> $fichier_temp_pour_retour_dialog
	}

	lit_retour_dialog()
	{
		valeur_retour_dialog=$?
		retour=$(cat $fichier_temp_pour_retour_dialog)
		echo retour
		DIALOG_OK=0
		DIALOG_CANCEL=1
		DIALOG_HELP=2
		DIALOG_EXTRA=3
		DIALOG_ITEM_HELP=4
		DIALOG_ESC=255

		case $valeur_retour_dialog in
		  $DIALOG_OK)
			retour=$(cat $fichier_temp_pour_retour_dialog)
			echo "$retour">&2;;
		  $DIALOG_CANCEL)
			echo "ANNULER">&2;;
		  $DIALOG_ESC)
			if test -s $fichier_temp_pour_retour_dialog ; then
				cat $fichier_temp_pour_retour_dialog
			else
				echo "ECHAP">&2
			fi
			;;
		esac
	}

	parse_arguments "${Arguments[@]}"
	construit_tableau
	prepare_dialog

	case $TYPE in
			"cases_a_cocher")
				affiche_dialog_checklist
				;;
			"bouton_radio")
				affiche_dialog_radiolist
				;;	
			"chaine")
				affiche_dialog_inputbox
				;;
			"ouinon")
				affiche_dialog_yesno
				;;
			"info")
				affiche_dialog_info
				;;
			*)
				echo $TYPE
				affiche_erreur_type
	esac

	lit_retour_dialog

# Vérification du résultat #

On active l'environnement virtuel de pelican :

	source ../staticblog/bin/activate

On lance la compilation et le serveur de dev :

	make html & make devserver &
	
On va voir le résultat sur 
	
	http://localhost:8000