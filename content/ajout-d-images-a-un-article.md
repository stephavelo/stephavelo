Title: Ajout d'images à un article
Slug: ajout-d-images-a-un-article
Date: 2019-07-27 21:25:01
Modified:  2019-07-27 21:25:01
Category: pelican
Tags: pelican

Authors: steph
Summary: Comment ajouter des images à un article sous Pélican


Comme je fais bon blog sur une forge git, j'aimerais héberger mes images sur un hébergeur d'images et les ajouter via des liens aux articles.

# Ajout manuel  #

## Image simple  ##

<p><img src="https://i.imgur.com/bN67UWW.png" alt="mon texte"/></p>

L'ajout d'une image se fait de la manière suivante :

- en Markdown :

		![Imgur](https://i.imgur.com/bN67UWW.png) 

- en HTML :

		<p><img src="https://i.imgur.com/bN67UWW.png" alt="mon texte"/></p>

## Miniature avec un lien vers l'image originale ##

==En ajoutant des images à l'article précédent j'ai découvert qu'en ajoutant au moins deux images par liens avec la syntaxe html Remarkable (l'éditeur Markdown que j'utilise) n'affiche pas la deuxième, donc j'utilise la syntaxe Markdown.==

<p><a href="https://i.imgur.com/01P28rT.png" title="texte lien vers l'image">
    <img src="https://i.imgur.com/01P28rTm.png" alt="texte alternatif" />
  </a></p>

- en Markdown :

		[![texte alternatif](https://i.imgur.com/01P28rTm.png)](https://i.imgur.com/01P28rT.png texte lien vers l'image)

- En HTML :

		<p><a href="https://i.imgur.com/01P28rT.png" title="texte lien vers l'image">
		    <img src="https://i.imgur.com/01P28rTm.png" alt="texte alternatif" />
		  </a></p>


# Automatisation #

Comme cette syntaxe est assez fastidieuse j'ai cherché à l'automatiser en :

- choisissant un hébergeur d'image pour lequel il existe un script d'upload en line de commande
- modifiant ce script pour obtenir directement le code à insérer dans l'article

## Choix du service et du script ##

- Sur [AskUbuntu](https://askubuntu.com/questions/544440/how-can-i-upload-a-photo-to-imgur-from-the-command-line-and-get-their-direct-lin) j'ai découvert [imgur.sh](https://github.com/tremby/imgur.sh) mais qui ne donne que le lien direct, et compliqué à adapter pour une  miniature liant l'image.

- J'ai ensuite découvert [Rugmi](https://github.com/RanyAlbegWein/Rugmi) qui a beaucoup d'options et qui m'a paru plus simple à adapter. C'est avec lui que j'ai inséré les images de cet article (et du précédent), après l'avoir un peu modifié.

Le service utilisé est donc [Imgur](https://imgur.com).

## Utilisation du script ##

Avec mes modifs :

		[sam. juil. 27][21:57:30] [~/Documents/blog/stephavelo] (master) 
		->  rugmi -f captures/creation_article_titre.png -t links -a "demande du titre"
		<p><a href="https://i.imgur.com/uRlwx8C.png" title="demande du titre">
		    <img src="https://i.imgur.com/uRlwx8Cm.png" alt="demande du titre" />
		  </a></p>
		
		[![demande du titre](https://i.imgur.com/uRlwx8Cm.png)](https://i.imgur.com/uRlwx8C.png demande du titre)
		[sam. juil. 27][21:58:02] [~/Documents/blog/stephavelo] (master) 



## Modifications du script ##

Ci-dessous les modifications :


- affichage du code pour une miniature liant l'image orginale
- ajout du texte alternatif en option pour insertion dans le code généré


		236a237,245
		>     
		> #~ https://i.imgur.com/r5N1io6
		> #~ https://i.imgur.com/r5N1io6t.png petit thumbnail
		> #~ https://i.imgur.com/r5N1io6m.png moyen thumbnail
		>     
		>     url_direct=${link}.png
		>     url_petit_thumbnail=${link}t.png
		>     url_moyen_thumbnail=${link}m.png  
		>     url_image_originale=${link}.png
		250a260,274
		>         HTML-link)
		> 			 printf '<p><a href="'"$url_direct"'" title="'"$alternative_text"'">
		>     <img src="'"$url_moyen_thumbnail"'" alt="'"$alternative_text"'" />
		>   </a></p>';;
		>         Markdown-link)
		> 			printf '[!['"$alternative_text"']('"$url_moyen_thumbnail"')]('"$url_image_originale"' '"$alternative_text"')';;
		> 		links)
		> 			printf '<p><a href="'"$url_image_originale"'" title="'"$alternative_text"'">
		>     <img src="'"$url_moyen_thumbnail"'" alt="'"$alternative_text"'" />
		>   </a></p>'
		> 			echo
		> 			echo
		> 			printf '[!['"$alternative_text"']('"$url_moyen_thumbnail"')]('"$url_direct"' '"$alternative_text"')'
		> 			echo;;
		> 			
		261c285
		<     [[ $1 = @(Image|Direct|Markdown|HTML|BBCode|LBBCode) ]]
		---
		>     [[ $1 = @(Image|Direct|Markdown|HTML|BBCode|LBBCode|Markdown-link|HTML-link|links) ]]
		440a465,469
		>         shift
		>         ;;
		>     -a|--alternative-text)
		>         #~ is_valid_display_size "$2" || die 2 "Inavlid display size: $2"
		>         alternative_text="$2"
		