Title: Neutralisation obligatoire du stationnement motorisé dans les 5 mètres avant un passage piéton d'ici fin 2026
Slug: neutralisation-du-stationnement-motorise-dans-les-5-metres-avant-un-passage-pieton-d-ici-fin-2026
Date: 2021-01-03 21:45:01
Modified:  2021-01-03 21:45:01
Category: vélo
Tags: piétons, vélo, visibilité

Authors: steph
Summary: Initialement recommandation du CEREMA puis obligation inscrite dans la loi LOM en 2019

# Recommandation du CEREMA...  #

[![Schéma du CEREMA](https://i.imgur.com/FZkrj9Bm.jpg)](https://i.imgur.com/FZkrj9B.jpg Schéma du CEREMA)

<!-- PELICAN_END_SUMMARY -->

[![Exemple à Rennes](https://i.imgur.com/g9KDPZHm.jpg)](https://i.imgur.com/g9KDPZHm.jpg Exemple à Rennes)

# ... maintenant dans la loi LOM #

> Art L. 118-5-1 du Code de la voirie routière
> 
> Afin d'assurer la sécurité des cheminements des piétons en établissant une meilleure visibilité mutuelle entre ces derniers et les véhicules circulant sur la chaussée, aucun emplacement de stationnement ne peut être aménagé sur la chaussée cinq mètres en amont des passages piétons, sauf si cet emplacement est réservé aux cycles et cycles à pédalage assisté ou aux engins de déplacement personnel.
> « Les dispositions du présent article sont applicables lors de la réalisation de travaux d'aménagement, de réhabilitation et de réfection des chaussées. Les travaux de mise en conformité doivent avoir été réalisés au plus tard le 31 décembre 2026. »
> (Article 52 de la LOM)

- [Fiche du CEREMA «Neutralisation du stationnement motorisé dans les 5 m en amont du passage piéton d’ici au 31 décembre 2026»](https://www.aitf.fr/system/files/files/pama10-covisibilitestationnement-2020-2.pdf) 
- [Loi d'orientation des mobilités : renforcer la place des mobilités actives et réduire les émissions de polluants (CEREMA)](https://www.cerema.fr/fr/actualites/loi-orientation-mobilites-renforcer-place-mobilites-actives) 
- [Fiches CEREMA plan d'actions pour les mobilités actives](https://www.cerema.fr/fr/actualites/toutes-fiches-plan-action-mobilites-actives-pama-telecharger) 
- [Article de la LOM date butoir](https://www.legifrance.gouv.fr/eli/loi/2019/12/24/2019-1428/jo/article_52) 
- [Amendement LOM date butoir](http://www.assemblee-nationale.fr/dyn/15/amendements/1831/CION-DVP/CD2885) 
