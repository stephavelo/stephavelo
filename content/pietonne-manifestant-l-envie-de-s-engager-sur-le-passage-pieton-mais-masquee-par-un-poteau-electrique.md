Title: Piétonne manifestant l'envie de s'engager sur le passage piéton mais masquée par un poteau électrique
Slug: pietonne-manifestant-l-envie-de-s-engager-sur-le-passage-pieton-mais-masquee-par-un-poteau-electrique
Date: 2021-01-03 21:40:04
Modified:  2021-01-03 21:40:04
Category: vélo
Tags: vélo, visibilité, piétons

Authors: steph
Summary: Photos montrant progressivement l'apparition d'une piétonne cachée par un poteau électrique devant une école à Yutz

[![Piétonne voulant traverser à un passage piéton mais masquée par un poteau électrique](https://i.imgur.com/JnhA8yYm.jpeg)](https://i.imgur.com/JnhA8yY.jpeg Piétonne voulant traverser à un passage piéton mais masquée par un poteau électrique)

<!-- PELICAN_END_SUMMARY -->

[![Piétonne voulant traverser à un passage piéton mais masquée par un poteau électrique](https://i.imgur.com/tujFJyGm.jpg)](https://i.imgur.com/tujFJyG.jpg Piétonne voulant traverser à un passage piéton mais masquée par un poteau électrique)

[![Piétonne voulant traverser à un passage piéton mais masquée par un poteau électrique](https://i.imgur.com/J0g7hzKm.jpg)](https://i.imgur.com/J0g7hzK.jpg Piétonne voulant traverser à un passage piéton mais masquée par un poteau électrique)
