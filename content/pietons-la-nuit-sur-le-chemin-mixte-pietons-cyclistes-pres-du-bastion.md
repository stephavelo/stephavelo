Title: Piétons la nuit sur le «chemin mixte piétons/cyclistes» près du Bastion
Slug: pietons-la-nuit-sur-le-chemin-mixte-pietons-cyclistes-pres-du-bastion
Date: 2021-01-03 20:36:32
Modified:  2021-01-03 20:36:32
Category: vélo
Tags: nuit, piétons, vélo, vélotaf, vélut, visibilité

Authors: steph
Summary: Piéton apparaissant progressivement sur les images de la GoPro

# Description de ce chemin d'après l'article de La Semaine paru après les résultats du Baromètre des villes cyclables

> Ici, c’est un passage assez étroit où les troncs des arbres ainsi que leurs racines prennent de plus en plus de place d’un côté, alors que de l’autre ce sont les pare-chocs des voitures qui s’avancent sur la voie, également empruntée par les piétons.
> 
> Les vélos y circulent dans les deux sens car il n’existe pas de piste à leur intention du côté opposé, le long de la Tour aux Puces et de l’Hôtel de Ville. 

[ Pistes cyclables : Thionville et Yutz doivent-elles changer de braquet ? ](https://www.lasemaine.fr/pistes-cyclables-thionville-yutz-doivent-elles-changer-braquet/) 

# Photos

[![Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit](https://i.imgur.com/QfR3H0Om.jpg)](https://i.imgur.com/QfR3H0O.jpg Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit)

<!-- PELICAN_END_SUMMARY -->

[![Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit](https://i.imgur.com/v9aDD67m.jpg)](https://i.imgur.com/v9aDD67.jpg Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit)

[![Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit](https://i.imgur.com/ENVphgVm.jpg)](https://i.imgur.com/ENVphgV.jpg Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit)

[![Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit](https://i.imgur.com/p7hZSIOm.jpg)](https://i.imgur.com/p7hZSIO.jpg Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit)

[![Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit](https://i.imgur.com/LbFixrqm.jpg)](https://i.imgur.com/LbFixrq.jpg Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit)

[![Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit](https://i.imgur.com/HI2igsUm.jpg)](https://i.imgur.com/HI2igsU.jpg Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit)

[![Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit](https://i.imgur.com/bTCE8GIm.jpg)](https://i.imgur.com/bTCE8GI.jpg Piétons sur le «chemin mixte piétons/cyclistes» près du Bastion, la nuit)
