Title: Cycliste avec phares et gilet jaune prenant l'avenue des Nations en venant de Basse-Ham sur la route pour tourner à gauche plus loin
Slug: cycliste-avec-phares-et-gilet-jaune-prenant-l-avenue-des-nations-en-venant-de-basse-ham-sur-la-route-pour-tourner-a-gauche-plus-loin
Date: 2021-01-03 22:11:48
Modified:  2021-01-03 22:11:48
Category: vélo
Tags: brillez, nuit, phares, vélo, vélotaf, vélut, visibilité

Authors: steph
Summary: Visibilité d'un cycliste avec phares et gilet jaune sur la route, de côté, derrière et avec les phares de voitures en face

[![Cycliste avec phares et gilet jaune venant de la droite sur la route](https://i.imgur.com/p6ZwuyAm.jpg)](https://i.imgur.com/p6ZwuyA.jpg Cycliste avec phares et gilet jaune venant de la droite sur la route)

<!-- PELICAN_END_SUMMARY -->

[![Cycliste avec phares et gilet jaune venant de la droite sur la route](https://i.imgur.com/hHTCES0m.jpg)](https://i.imgur.com/hHTCES0.jpg Cycliste avec phares et gilet jaune venant de la droite sur la route)

[![Cycliste avec phares et gilet jaune venant de la droite sur la route](https://i.imgur.com/ovIaiRMm.jpg)](https://i.imgur.com/ovIaiRM.jpg Cycliste avec phares et gilet jaune venant de la droite sur la route)

[![Cycliste avec phares et gilet jaune invisible sur la GoPro car dans les phares de voiture](https://i.imgur.com/BswcvGZm.jpg)](https://i.imgur.com/BswcvGZ.jpg Cycliste avec phares et gilet jaune invisible sur la GoPro car dans les phares de voiture)

[![Cycliste avec phares et gilet jaune, seul le feu arrière est visible avec les phares de voiture](https://i.imgur.com/sJtlQZzm.jpeg)](https://i.imgur.com/sJtlQZz.jpeg Cycliste avec phares et gilet jaune, seul le feu arrière est visible avec les phares de voiture)

[![Cycliste avec phares et gilet jaune, on voit surtout le feu arrière](https://i.imgur.com/lLUIvuQm.jpeg)](https://i.imgur.com/lLUIvuQ.jpeg Cycliste avec phares et gilet jaune, on voit surtout le feu arrière)