#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
LOAD_CONTENT_CACHE = False
AUTHOR = 'steph'
SITENAME = 'Steph à vélo'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# By default Pelican puts the generated files into output folder and 
# this won't work in GitLab because it requires static files to be 
# located in public folder.
# https://mister-gold.pro/posts/en/deploy-pelican-on-gitlab-pages/
OUTPUT_PATH = 'public/'


PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['pelican-toc', 'video_privacy_enhancer', 'tag_cloud', 'summary']
#~ http://pirsquared.org/blog/pelican-transition.html#enabling-table-of-contents-for-posts
#~ If you want to include a table of contents within a post using [TOC], you must enabled the markdown toc processor with a line like this is your pelicanconf.py:
#~ MD_EXTENSIONS =  [ 'toc', 'codehilite','extra']
MARKDOWN = {
  'extension_configs': {
    'markdown.extensions.toc': {
      'title': 'Table of contents:' 
    },
    'markdown.extensions.codehilite': {'css_class': 'highlight'},
    'markdown.extensions.extra': {},
    'markdown.extensions.meta': {},
  },
  'output_format': 'html5',
}

SUMMARY_USE_FIRST_PARAGRAPH = True




#~ THEME = 'themes/pelican-alchemy/alchemy'
#~ THEME = 'themes/mytheme'
THEME = 'themes/pelican_mygeekdaddy/mygeekdaddy'
# http://docs.getpelican.com/en/stable/settings.html
#~ Basic settings
USE_FOLDER_AS_CATEGORY = True

    #~ When you don’t specify a category in your post metadata, set this setting to True, and organize your articles in subfolders, the subfolder will become the category of your post. If set to False, DEFAULT_CATEGORY will be used as a fallback.

DEFAULT_CATEGORY = 'misc'

    #~ The default category to fall back on.

DISPLAY_PAGES_ON_MENU = True

    #~ Whether to display pages on the menu of the template. Templates may or may not honor this setting.

DISPLAY_CATEGORIES_ON_MENU = True

    #~ Whether to display categories on the menu of the template. Templates may or not honor this setting.

#~ DOCUTILS_SETTINGS = {}

    #~ Extra configuration settings for the docutils publisher (applicable only to reStructuredText). See Docutils Configuration settings for more details.

DELETE_OUTPUT_DIRECTORY = False

    #~ Delete the output directory, and all of its contents, before generating new files. This can be useful in preventing older, unnecessary files from persisting in your output. However, this is a destructive setting and should be handled with extreme care.


#~ SLUGIFY_SOURCE = 'title'

    #~ Specifies where you want the slug to be automatically generated from. Can be set to title to use the ‘Title:’ metadata tag or basename to use the article’s file name when creating the slug.

#~ You can customize the URLs and locations where files will be saved. The *_URL and *_SAVE_AS variables use Python’s format strings. These variables allow you to place your articles in a location such as {slug}/index.html and link to them as {slug} for clean URLs (see example below). These settings give you the flexibility to place your articles and pages anywhere you want.

#~ Note

#~ If you specify a datetime directive, it will be substituted using the input files’ date metadata attribute. If the date is not specified for a particular file, Pelican will rely on the file’s mtime timestamp. Check the Python datetime documentation for more information.

#~ Also, you can use other file metadata attributes as well:

    #~ slug
    #~ date
    #~ lang
    #~ author
    #~ category

#~ Example usage:

#~ ARTICLE_URL = 'posts/{date:%Y}/{date:%b}/{date:%d}/{slug}/'
#~ ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/{date:%d}/{slug}/index.html'
#~ PAGE_URL = 'pages/{slug}/'
#~ PAGE_SAVE_AS = 'pages/{slug}/index.html'

#~ This would save your articles into something like /posts/2011/Aug/07/sample-post/index.html, save your pages into /pages/about/index.html, and render them available at URLs of /posts/2011/Aug/07/sample-post/ and /pages/about/, respectively.


#~ RELATIVE_URLS = False

    #~ Defines whether Pelican should use document-relative URLs or not. Only set this to True when developing/testing and only if you fully understand the effect it can have on links/feeds.

#~ ARTICLE_URL = '{slug}.html'

    #~ The URL to refer to an article.

#~ ARTICLE_SAVE_AS = '{slug}.html'

    #~ The place where we will save an article.

#~ ARTICLE_LANG_URL = '{slug}-{lang}.html'

    #~ The URL to refer to an article which doesn’t use the default language.

#~ ARTICLE_LANG_SAVE_AS = '{slug}-{lang}.html'

    #~ The place where we will save an article which doesn’t use the default language.

#~ DRAFT_URL = 'drafts/{slug}.html'

    #~ The URL to refer to an article draft.

#~ DRAFT_SAVE_AS = 'drafts/{slug}.html'

    #~ The place where we will save an article draft.

#~ DRAFT_LANG_URL = 'drafts/{slug}-{lang}.html'

    #~ The URL to refer to an article draft which doesn’t use the default language.

#~ DRAFT_LANG_SAVE_AS = 'drafts/{slug}-{lang}.html'

    #~ The place where we will save an article draft which doesn’t use the default language.

#~ PAGE_URL = 'pages/{slug}.html'

    #~ The URL we will use to link to a page.

#~ PAGE_SAVE_AS = 'pages/{slug}.html'

    #~ The location we will save the page. This value has to be the same as PAGE_URL or you need to use a rewrite in your server config.

#~ PAGE_LANG_URL = 'pages/{slug}-{lang}.html'

    #~ The URL we will use to link to a page which doesn’t use the default language.

#~ PAGE_LANG_SAVE_AS = 'pages/{slug}-{lang}.html'

    #~ The location we will save the page which doesn’t use the default language.

#~ DRAFT_PAGE_URL = 'drafts/pages/{slug}.html'

    #~ The URL used to link to a page draft.

#~ DRAFT_PAGE_SAVE_AS = 'drafts/pages/{slug}.html'

    #~ The actual location a page draft is saved at.

#~ DRAFT_PAGE_LANG_URL = 'drafts/pages/{slug}-{lang}.html'

    #~ The URL used to link to a page draft which doesn’t use the default language.

#~ DRAFT_PAGE_LANG_SAVE_AS = 'drafts/pages/{slug}-{lang}.html'

    #~ The actual location a page draft which doesn’t use the default language is saved at.

#~ CATEGORY_URL = 'category/{slug}.html'

    #~ The URL to use for a category.

#~ CATEGORY_SAVE_AS = 'category/{slug}.html'

    #~ The location to save a category.

#~ TAG_URL = 'tag/{slug}.html'

    #~ The URL to use for a tag.

#~ TAG_SAVE_AS = 'tag/{slug}.html'

    #~ The location to save the tag page.

#~ AUTHOR_URL = 'author/{slug}.html'

    #~ The URL to use for an author.

#~ AUTHOR_SAVE_AS = 'author/{slug}.html'

    #~ The location to save an author.

#~ YEAR_ARCHIVE_SAVE_AS = ''

    #~ The location to save per-year archives of your posts.

#~ YEAR_ARCHIVE_URL = ''

    #~ The URL to use for per-year archives of your posts. Used only if you have the {url} placeholder in PAGINATION_PATTERNS.

#~ MONTH_ARCHIVE_SAVE_AS = ''

    #~ The location to save per-month archives of your posts.

#~ MONTH_ARCHIVE_URL = ''

    #~ The URL to use for per-month archives of your posts. Used only if you have the {url} placeholder in PAGINATION_PATTERNS.

#~ DAY_ARCHIVE_SAVE_AS = ''

    #~ The location to save per-day archives of your posts.

#~ DAY_ARCHIVE_URL = ''

    #~ The URL to use for per-day archives of your posts. Used only if you have the {url} placeholder in PAGINATION_PATTERNS.

#~ Note

#~ If you do not want one or more of the default pages to be created (e.g., you are the only author on your site and thus do not need an Authors page), set the corresponding *_SAVE_AS setting to '' to prevent the relevant page from being generated.


#~ Time and Date

#~ TIMEZONE

    #~ The timezone used in the date information, to generate Atom and RSS feeds.

    #~ If no timezone is defined, UTC is assumed. This means that the generated Atom and RSS feeds will contain incorrect date information if your locale is not UTC.

    #~ Pelican issues a warning in case this setting is not defined, as it was not mandatory in previous versions.

    #~ Have a look at the wikipedia page to get a list of valid timezone values.

#~ DEFAULT_DATE = None

    #~ The default date you want to use. If 'fs', Pelican will use the file system timestamp information (mtime) if it can’t get date information from the metadata. If given any other string, it will be parsed by the same method as article metadata. If set to a tuple object, the default datetime object will instead be generated by passing the tuple to the datetime.datetime constructor.

#~ DEFAULT_DATE_FORMAT = '%a %d %B %Y'

    #~ The default date format you want to use.

#~ DATE_FORMATS = {}

    #~ If you manage multiple languages, you can set the date formatting here.

    #~ If no DATE_FORMATS are set, Pelican will fall back to DEFAULT_DATE_FORMAT. If you need to maintain multiple languages with different date formats, you can set the DATE_FORMATS dictionary using the language name (lang metadata in your post content) as the key.

    #~ In addition to the standard C89 strftime format codes that are listed in Python strftime documentation, you can use the - character between % and the format character to remove any leading zeros. For example, %d/%m/%Y will output 01/01/2014 whereas %-d/%-m/%Y will result in 1/1/2014.

    #~ DATE_FORMATS = {
        #~ 'en': '%a, %d %b %Y',
        #~ 'jp': '%Y-%m-%d(%a)',
    #~ }

    #~ It is also possible to set different locale settings for each language by using a (locale, format) tuple as a dictionary value which will override the LOCALE setting:

    #~ # On Unix/Linux
    #~ DATE_FORMATS = {
        #~ 'en': ('en_US','%a, %d %b %Y'),
        #~ 'jp': ('ja_JP','%Y-%m-%d(%a)'),
    #~ }

    #~ # On Windows
    #~ DATE_FORMATS = {
        #~ 'en': ('usa','%a, %d %b %Y'),
        #~ 'jp': ('jpn','%Y-%m-%d(%a)'),
    #~ }


#~ Metadata

#~ AUTHOR

    #~ Default author (usually your name).

#~ DEFAULT_METADATA = {}

    #~ The default metadata you want to use for all articles and pages.

#~ FILENAME_METADATA = r'(?P<date>d{4}-d{2}-d{2}).*'

    #~ The regexp that will be used to extract any metadata from the filename. All named groups that are matched will be set in the metadata object. The default value will only extract the date from the filename.

    #~ For example, to extract both the date and the slug:

    #~ FILENAME_METADATA = r'(?P<date>\d{4}-\d{2}-\d{2})_(?P<slug>.*)'

    #~ See also SLUGIFY_SOURCE.

#~ PATH_METADATA = ''

    #~ Like FILENAME_METADATA, but parsed from a page’s full path relative to the content source directory.

#~ EXTRA_PATH_METADATA = {}

    #~ Extra metadata dictionaries keyed by relative path. Relative paths require correct OS-specific directory separators (i.e. / in UNIX and \ in Windows) unlike some other Pelican file settings.

#~ Not all metadata needs to be embedded in source file itself. For example, blog posts are often named following a YYYY-MM-DD-SLUG.rst pattern, or nested into YYYY/MM/DD-SLUG directories. To extract metadata from the filename or path, set FILENAME_METADATA or PATH_METADATA to regular expressions that use Python’s group name notation (?P<name>…). If you want to attach additional metadata but don’t want to encode it in the path, you can set EXTRA_PATH_METADATA:

#~ EXTRA_PATH_METADATA = {
    #~ 'relative/path/to/file-1': {
        #~ 'key-1a': 'value-1a',
        #~ 'key-1b': 'value-1b',
        #~ },
    #~ 'relative/path/to/file-2': {
        #~ 'key-2': 'value-2',
        #~ },
    #~ }

#~ This can be a convenient way to shift the installed location of a particular file:

#~ # Take advantage of the following defaults
#~ # STATIC_SAVE_AS = '{path}'
#~ # STATIC_URL = '{path}'
STATIC_PATHS = [
    'images',
    ]
#~ EXTRA_PATH_METADATA = {
    #~ 'static/robots.txt': {'path': 'robots.txt'},
    #~ }

# code blocks with line numbers
PYGMENTS_RST_OPTIONS = {'linenos': 'table'}
# http://docs.getpelican.com/en/stable/content.html#translations
DEFAULT_LANG="fr"
# j'invente un tag
DISPLAY_TAGS_ON_MENU = True
TAG_CLOUD_STEPS = 4
TAG_CLOUD_MAX_ITEMS = 100
TAG_CLOUD_SORTING = 'random'

TAG_CLOUD_BADGE = False
TAG_CLOUD_CSS_FILE = 'tagcloud.css'
