#!/bin/bash
#~ https://ftp.traduc.org/doc-vf/gazette-linux/html/2004/101/lg101-P.html
#~ http://linuxcommand.org/lc3_adv_dialog.php

#~ parser d'arguments
#~ https://gist.github.com/jehiah/855086
#~ https://stackoverflow.com/questions/18544359/how-to-read-user-input-into-a-variable-in-bash

escape()
{
	x=$(printf '%q' "$1")
	echo $x
}

usage()
{
    echo "Demande d'une chaine de caracteres"
    echo ""
    echo "$0"
    echo  "\t-h --help"
    echo  "\t-t --titre=$TITRE"
    echo  "\t--type=$TYPE (ouinon, chaine, bouton_radio ou cases_a_cocher)"
    echo  "\t-m --message=$MESSAGE (insérer § pour passer au paragraphe suivant)"
    echo ""
}

#~ https://ubuntuforums.org/showthread.php?t=2082936
Arguments=( "$@" )  # Arguments=( "a b" "c" "d e" )


parse_arguments()
{
	while [ "$1" != "" ]; do
		PARAM=`echo $1 | awk -F= '{print $1}'`
		VALUE=`echo $1 | awk -F= '{print $2}'`
		case $PARAM in
			-h | --help)
				usage
				exit
				;;
			--titre | -t)
				TITRE=$VALUE
				;;
			--type)
				TYPE=$VALUE
				;;
			--message | -m)
				MESSAGE="$VALUE"
				#~ escape "$MESSAGE"
				MESSAGE=$(echo "$MESSAGE"|sed 's/§/\n - /g')
				#~ escape "$MESSAGE"
				;;
			--liste | -l)
			#~ ne gere pas les espaces
				ELEMENTS="$VALUE"
				;;
			*)
				echo "ERROR: unknown parameter \"$PARAM\""
				usage
				exit 1
				;;
		esac
		shift
	done	
}

construit_tableau()
{
	ELEMENTS=$(echo $ELEMENTS|tr '§' '\n')
	NB_ITEMS=1
	while read -r elt
	do
		LISTE+=("$elt")
		LISTE+=("$elt")
		LISTE+=("off")
	   
		NB_ITEMS=$((NB_ITEMS+1))
	done <<< "$ELEMENTS"
}

prepare_dialog()
{
	DIALOG=${DIALOG=dialog}
	HAUTEUR=20
	LARGEUR=51

	fichier_temp_pour_retour_dialog=`tempfile 2>/dev/null` || fichier_temp_pour_retour_dialog=/tmp/fichier_temp_pour_retour_dialog$$
	trap "rm -f $fichier_temp_pour_retour_dialog" 0 1 2 5 15
}

affiche_dialog_checklist()
{
	$DIALOG  \
		--backtitle "$TITRE"  \
		--title "$TITRE"  \
		--clear \
		--checklist "$MESSAGE" \
			$HAUTEUR $LARGEUR $NB_ITEMS \
			"${LISTE[@]}" \
			2> $fichier_temp_pour_retour_dialog
}

affiche_dialog_radiolist()
{
	$DIALOG  \
		--backtitle "$TITRE"  \
		--title "$TITRE"  \
		--clear \
		--radiolist "$MESSAGE" \
			$HAUTEUR $LARGEUR $NB_ITEMS \
			"${LISTE[@]}" \
			2> $fichier_temp_pour_retour_dialog
}

affiche_dialog_menu()
{
	$DIALOG  \
		--backtitle "$TITRE"  \
		--title "$TITRE"  \
		--clear \
		--menubox "$MESSAGE" \
			$HAUTEUR $LARGEUR $NB_ITEMS \
			"${LISTE[@]}" \
			2> $fichier_temp_pour_retour_dialog
}

affiche_dialog_inputbox()
{
	$DIALOG  \
		--backtitle "$TITRE"  \
		--title "$TITRE"  \
		--clear \
		--inputbox "$MESSAGE" \
			$HAUTEUR $LARGEUR \
			2> $fichier_temp_pour_retour_dialog
}

affiche_dialog_yesno()
{
	$DIALOG  \
		--backtitle "$TITRE"  \
		--title "$TITRE"  \
		--clear \
		--yesno "$MESSAGE" \
			$HAUTEUR $LARGEUR \
			2> $fichier_temp_pour_retour_dialog
}

affiche_erreur_type()
{
	$DIALOG  \
		--backtitle "Erreur de type"  \
		--title "Erreur de type"  \
		--clear \
		--msgbox "type $TYPE inexistant." \
			$HAUTEUR $LARGEUR \
			2> $fichier_temp_pour_retour_dialog
}


affiche_dialog_info()
{
	$DIALOG  \
		--backtitle "$TITRE"  \
		--title "$TITRE"  \
		--clear \
		--msgbox "$MESSAGE" \
			$HAUTEUR $LARGEUR  \
			2> $fichier_temp_pour_retour_dialog
}

lit_retour_dialog()
{
	valeur_retour_dialog=$?
	retour=$(cat $fichier_temp_pour_retour_dialog)
	echo retour
	DIALOG_OK=0
	DIALOG_CANCEL=1
	DIALOG_HELP=2
	DIALOG_EXTRA=3
	DIALOG_ITEM_HELP=4
	DIALOG_ESC=255

	case $valeur_retour_dialog in
	  $DIALOG_OK)
		retour=$(cat $fichier_temp_pour_retour_dialog)
		echo "$retour">&2;;
	  $DIALOG_CANCEL)
		echo "ANNULER">&2;;
	  $DIALOG_ESC)
		if test -s $fichier_temp_pour_retour_dialog ; then
			cat $fichier_temp_pour_retour_dialog
		else
			echo "ECHAP">&2
		fi
		;;
	esac
}

parse_arguments "${Arguments[@]}"
construit_tableau
prepare_dialog

case $TYPE in
		"cases_a_cocher")
			affiche_dialog_checklist
			;;
		"bouton_radio")
			affiche_dialog_radiolist
			;;	
		"chaine")
			affiche_dialog_inputbox
			;;
		"ouinon")
			affiche_dialog_yesno
			;;
		"info")
			affiche_dialog_info
			;;
		*)
			echo $TYPE
			affiche_erreur_type
esac

lit_retour_dialog
