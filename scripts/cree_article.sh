#!/bin/bash
#~ https://ftp.traduc.org/doc-vf/gazette-linux/html/2004/101/lg101-P.html
#~ http://linuxcommand.org/lc3_adv_dialog.php

chemin_script=$(dirname "$0")

########################################################################
#                          FONCTIONS                                   #
########################################################################


#~ https://stackoverflow.com/questions/5608112/escape-filenames-the-same-way-bash-does-it
escape()
{
	x=$(printf '%q' "$1")
	echo $x
}

#~ ---------------------------------------------------------------------
#~ |                 lancement de dialog                               |
#~ ---------------------------------------------------------------------

cree_fichier_retour_dialog()
{
	fichier_temp_pour_retour_tags=`tempfile 2>/dev/null` || fichier_temp_pour_retour_tags=/tmp/fichier_temp_pour_retour_tags$$
	trap "rm -f $fichier_temp_pour_retour_dialog" 0 1 2 5 15
}

lance_dialog()
{
	cree_fichier_retour_dialog
}

affiche_info()
{
	titre_dialog="$1"
	message_dialog="$2"
	cree_fichier_retour_dialog
	${chemin_script}/lance_dialog.sh --type=info --titre="$titre_dialog" -m="$message_dialog" 2> $fichier_temp_pour_retour_tags
}

demande_chaine()
{
	titre_dialog="$1"
	message_dialog="$2"
	cree_fichier_retour_dialog
	${chemin_script}/lance_dialog.sh --type=chaine --titre="$titre_dialog" -m="$message_dialog" 2> $fichier_temp_pour_retour_tags
}

demande_ouinon()
{
	titre_dialog="$1"
	message_dialog="$2"
	cree_fichier_retour_dialog
	${chemin_script}/lance_dialog.sh --type=ouinon --titre="$titre_dialog" -m="$message_dialog" 2> $fichier_temp_pour_retour_tags
}

demande_selection_multiple()
{
	titre_dialog="$1"
	message_dialog="$2"
	liste_dialog="$3"
	cree_fichier_retour_dialog
	${chemin_script}/lance_dialog.sh --type=cases_a_cocher --titre="$titre_dialog" -m="$message" --liste="$liste_dialog" 2> $fichier_temp_pour_retour_tags
}

demande_selection_unique()
{
	titre_dialog="$1"
	message_dialog="$2"
	liste_dialog="$3"
	cree_fichier_retour_dialog
	${chemin_script}/lance_dialog.sh --type=bouton_radio --titre="$titre_dialog" -m="$message" --liste="$liste_dialog" 2> $fichier_temp_pour_retour_tags
}

#~ ---------------------------------------------------------------------
#~ |          demande des propriétés du nouvel article                 |
#~ ---------------------------------------------------------------------

traite_retour()
{
	case "$1" in
			"ANNULER")
				echo ANNULER
				exit 1
				;;
			"ECHAP")
				echo ECHAP
				exit 1
				;;	
	esac	
}


#~ TITRE
demande_titre()
{
	demande_chaine "Titre :" "Veuillez donner le titre."
	titre=$(cat $fichier_temp_pour_retour_tags)
	traite_retour "$titre"
}

#~ RESUME
demande_resume()
{
	demande_chaine "Résumé :" "Veuillez donner le résumé."
	resume=$(cat $fichier_temp_pour_retour_tags)
	traite_retour "$resume"
}

#~ TAGS
demande_tags()
{
	tags_pour_dialog=$(grep Tags\: content/*|cut -d: -f3-10 | tr -d ','| tr ' ' '\n'|sort|uniq|sed '/^$/d'|tr '\n' '§')
	demande_selection_multiple "Tags :" "Veuillez sélectionner les tags." "$tags_pour_dialog"
	tags=$(cat $fichier_temp_pour_retour_tags)
	tags=$(echo "$tags"|sed 's/ /, /g')
	traite_retour "$tags"
}
#~ CATEGORY
demande_categorie()
{
	categories_pour_dialog=$(grep Category\: content/*|cut -d: -f3-10 | tr -d ','| tr ' ' '\n'|sort|uniq|sed '/^$/d'|tr '\n' '§')
	demande_selection_unique "Catégorie :" "Veuillez sélectionner la catégorie." "$categories_pour_dialog"
	categorie=$(cat $fichier_temp_pour_retour_tags)
	traite_retour "$categorie"
}
#~ AUTEUR
demande_auteur()
{
	auteurs_pour_dialog=$(grep Authors\: content/*|cut -d: -f3-10 | tr -d ','| tr ' ' '\n'|sort|uniq|sed '/^$/d'|tr '\n' '§')
	demande_selection_unique "Auteur :" "Veuillez sélectionner l'auteur." "$auteurs_pour_dialog"
	auteur=$(cat $fichier_temp_pour_retour_tags)
	traite_retour "$auteur"
}

prepare_date()
{
	#~ https://www.cyberciti.biz/faq/linux-unix-formatting-dates-for-display/
	date_creation=$(date +"%Y-%m-%d")
	heure_creation=$(date +"%T")
	echo $date_creation
	echo $heure_creation
}

prepare_slug()
{
	#~ https://serverfault.com/questions/348482/how-to-remove-invalid-characters-from-filenames
	slug=$(echo "$titre" | sed -e 's/[^A-Za-z0-9._-]/_/g')
	#~ https://automatthias.wordpress.com/2007/05/21/slugify-in-a-shell-script/
	#~ https://www.admin-linux.fr/bash-suppression-des-accents-cedilles-etc/
	slug=$(echo -n "${titre}" | sed -e 's/[^[:alnum:]]/-/g' | tr -s '-' | tr A-Z a-z | iconv -f utf8 -t ascii//TRANSLIT)
 	echo $slug
}

demande_validation()
{
	echo $slug
	demande_ouinon "Validation :" "Est-ce que toutes les informations ci-dessous sont correctes ?
	§titre : $titre
	§slug : $slug
	§categorie : $categorie
	§tags : $tags
	§auteur : $auteur
	§resume : $resume
	§date : $date_creation
	§heure : $heure_creation
	"
	confirm=$(cat $fichier_temp_pour_retour_tags)
	case $confirm in
			"ANNULER")
				echo ANNULER
				exit 1
				;;
			"ECHAP")
				echo ECHAP
				exit 1
				;;	
			"")
				echo CONTINUE
				;;
			*)
				affiche_info "cas inconnu" "confirmation inconnue : $confirm"
	esac
}



#~ ---------------------------------------------------------------------
#~ |          demande des propriétés du nouvel article                 |
#~ ---------------------------------------------------------------------
copie_modele()
{

	CHEMIN_MODELE=modeles_pages/modele.md
	CHEMIN_ARTICLE=content/${slug}.md
	
	cp ${CHEMIN_MODELE} ${CHEMIN_ARTICLE}
	
	sed -i -- 's#<titre>#'"$titre"'#' ${CHEMIN_ARTICLE}
	sed -i -- 's#<slug>#'"$slug"'#' ${CHEMIN_ARTICLE}
	sed -i -- 's#<date>#'"$date_creation"'#' ${CHEMIN_ARTICLE}
	sed -i -- 's#<heure>#'"$heure_creation"'#' ${CHEMIN_ARTICLE}
	sed -i -- 's#<categorie>#'"$categorie"'#' ${CHEMIN_ARTICLE}
	sed -i -- 's#<tags>#'"$tags"'#' ${CHEMIN_ARTICLE}
	sed -i -- 's#<auteur>#'"$auteur"'#' ${CHEMIN_ARTICLE}
	sed -i -- 's#<resume>#'"$resume"'#' ${CHEMIN_ARTICLE}
	
	remarkable 	${CHEMIN_ARTICLE} &
}



########################################################################
#                           SCRIPT                                     #
########################################################################
#~ Crée une nouvelle page/un nouvel article

demande_titre
prepare_slug
demande_auteur
demande_resume
demande_tags
demande_categorie
prepare_date
demande_validation
copie_modele

